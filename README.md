Van Mark Apartments in Monroe, Louisiana offers 1- and 2-bedroom units that feature a private balcony/patio with extra storage, electric appliances, walk-in closets, and plank flooring. Our community amenities include a swimming pool, laundry facility, fitness center and more!

Address: 3980 Old Sterlington Road, Monroe, LA 71203, USA
Phone: 318-345-2256
